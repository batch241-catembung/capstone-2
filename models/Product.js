const mongoose = require("mongoose");

const courseSchema = new mongoose.Schema({
	name: 
		{
		type: String,
		required: [true, "Name is required"]
		},

	description: 
		{
		type: String,
		required: [true, "Description is required"]
		},

	price:
	 	{
		type: Number,
		required: [true, "Price is required"]
		},

	isActive: 
		{
		type: Boolean
		},

	createdOn: 
		{
		type: Date,
		default: new Date()
		},

	ordersProduct:
		[{
	orderId: 
			{
			type: String,
			required: [true, "orderId is required"]
			},	
		}]

})
module.exports = mongoose.model("Product", courseSchema);