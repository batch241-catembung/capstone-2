const mongoose = require("mongoose");

const courseSchema = new mongoose.Schema({

	name: 
		{
		type: String,
		required: [true, "Name is required"]
		},

	email: 
		{
		type: String,
		required: [true, "Email is required"]
		},

	password:
	 	{
		type: String,
		required: [true, "Password is required"]
		},	

	contactNo: 
		{
		type: String,
		required: [true, "Contact No. is required"]
		},	

	address: 
		{
		type: String,
		required: [true, "Address is required"]
		},

	isAdmin: 
		{
		type: Boolean,
		default: false
		},

	ordersUser: 
		[{
			products:[{
						productId: {type: String},
						productName:{type: String},
						quantity:{type: Number}
					}],
			totalAmount:{type: Number},

			purchaseOn:
			{
			type:Date,
			default: new Date()
			}
		}]

})
module.exports = mongoose.model("User", courseSchema);
