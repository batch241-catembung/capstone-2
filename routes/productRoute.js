const express =require("express");

const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth");

//create Product
router.post("/createProduct", (req, res)=>{
	const data = {
		product: req.body,
	}
	productController.createProduct(data).then(resultFromController=>res.send(resultFromController));

});
// router.post("/createProduct", auth.verify, (req, res)=>{
// const data = {
// 		product: req.body,
// 		isAdmin: auth.decode(req.headers.authorization).isAdmin
// 	}
// 	productController.createProduct(data).then(resultFromController=>res.send(resultFromController));

// });

//retrieve all products
router.get("/retrieveAllProduct", (req, res)=>{
	productController.retrieveAllProduct().then(resultFromController => res.send(resultFromController));
});

//retrieve all active products
router.get("/retrieveAllActiveProduct", (req, res)=>{
	productController.retrieveAllActiveProduct().then(resultFromController => res.send(resultFromController));
});

//retrieve single products
router.get("/retrieveOneProduct/:productId", (req, res)=>{
	productController.retrieveOneProduct(req.params).then(resultFromController => res.send(resultFromController));
});

//update product
router.put("/updateProduct/:productId", (req, res) => {
	const data = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price,
		isActive:req.body.isActive
	}
	console.log("route")
	console.log(data)
	productController.updateProduct(data, req.params).then(resultFromController => res.send(resultFromController));
});
// //update product
// router.put("/updateProduct", auth.verify, (req, res) => {
// 	const data = {
// 		isAdmin: auth.decode(req.headers.authorization).isAdmin,
// 		productId: req.body.productId,
// 		name: req.body.name,
// 		description: req.body.description,
// 		price: req.body.price
// 	}
// 	productController.updateProduct(data).then(resultFromController => res.send(resultFromController));
// });
// router.put("/updateProduct/:userId/:productId", auth.verify, (req, res) => {
// 	productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
// });

// Archive a product
router.patch("/archive", auth.verify, (req, res) => {
		const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		productId: req.body.productId
	}
	productController.archiveProduct(data).then(resultFromController => res.send(resultFromController));
});
// router.patch("/archive/:productId", auth.verify, (req, res) => {
// 	productController.archiveProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
// });

//check email
router.post("/checkProduct", (req, res)=>{
	productController.checkProductExist(req.body).then(resultFromController=>res.send(resultFromController));
});



module.exports = router;