const express =require("express");

const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");

//check email
router.post("/checkEmail", (req, res)=>{
	userController.checkEmailExist(req.body).then(resultFromController=>res.send(resultFromController));
});

//register user
router.post("/register", (req, res)=>{
	userController.registerUser(req.body).then(resultFromController=>res.send(resultFromController));
});

//login
router.post("/login", (req, res)=>{
	userController.loginUser(req.body).then(resultFromController=>res.send(resultFromController));
})

//create order nonAdmin only
router.post("/createOrder/:productId", auth.verify, (req, res) => {
	const data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		productName: req.body.productName,
		quantity: req.body.quantity,
		totalAmount: req.body.totalAmount
	};
	console.log("user route")
	console.log(data)
	userController.createOrder(data, req.params).then(resultFromController => res.send(resultFromController));
});
// router.post("/createOrder", auth.verify, (req, res) => {
// 	const data = {
// 		isAdmin: auth.decode(req.headers.authorization).isAdmin,
// 		userId: auth.decode(req.headers.authorization).id,
// 		productId: req.body.productId,
// 		productName: req.body.productName,
// 		quantity: req.body.quantity,
// 		totalAmount: req.body.totalAmount
// 	};
// 	console.log("user route")
// 	console.log(data)
// 	userController.createOrder(data).then(resultFromController => res.send(resultFromController));
// });
//Retrieve my User details
router.get("/retrieveUser", auth.verify, (req, res)=>{
	const data = {
		userId: auth.decode(req.headers.authorization).id
	}
	userController.retrieveUser(data).then(resultFromController => res.send(resultFromController));
});

//Retrieve All User
router.get("/retrieveAllUser", (req, res)=>{
	userController.retrieveAllUser().then(resultFromController => res.send(resultFromController));
});

//Retrieve All User Admin Only
router.get("/retrieveAllUserAdmin", auth.verify, (req, res)=>{
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	userController.retrieveAllUserAdmin(data).then(resultFromController => res.send(resultFromController));
});

//Retrieve authenticated user’s orders
router.get("/retrieveUserOrder", auth.verify, (req, res)=>{
	const data = {
		userId: auth.decode(req.headers.authorization).id };
	userController.retrieveUserOrder(data).then(resultFromController => res.send(resultFromController));
});

//set Admin user
router.patch("/setAdmin", auth.verify, (req, res)=>{
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		newAdmin: req.body.userId }
	userController.setAdmin(data).then(resultFromController => res.send(resultFromController));
});

//retrieve all orders
// router.get("/retrieveallorders", auth.verify, (req, res)=>{
// 	const data = {
// 		isAdmin: auth.decode(req.headers.authorization).isAdmin,
// 	}
// 	userController.retrieveallorders(data).then(resultFromController => res.send(resultFromController));
// });

//retrieve my own orders
router.get("/retrievemyorders", auth.verify, (req, res)=>{
	const data = {
			userId: auth.decode(req.headers.authorization).id };
	userController.retrievemyorders(data).then(resultFromController => res.send(resultFromController));
});

// Retreive user details (For Frontend)
router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
		console.log(userData)
		console.log(req.headers.authorization);
	userController.getProfile({id: userData.id}).then(resultFromController => res.send(resultFromController))
});



module.exports = router;