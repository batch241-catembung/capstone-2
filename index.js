const express = require("express");
const mongoose = require("mongoose");
//allows our backend application to be available to our frontend application
const cors = require("cors");

const userRoute = require("./routes/userRoute");
const productRoute = require("./routes/productRoute");

const port = 4000;

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

mongoose.set('strictQuery', false);

mongoose.connect("mongodb+srv://admin123:admin123@cluster0.ix4q3te.mongodb.net/CAPSTONE2?retryWrites=true&w=majority",{

	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", ()=> console.log("Successfully Connected to MongoDB Atlas"));

app.use("/users", userRoute);
app.use("/products", productRoute);


app.listen(port, ()=> {
	console.log(`API is now online on port: ${port}`)
})