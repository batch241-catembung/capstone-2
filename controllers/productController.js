const Product = require("../models/Product");
const User = require("../models/User");



module.exports.createProduct = (data) => {
		let newProduct = new Product({
			name: data.product.name,
			description: data.product.description,
			price: data.product.price,
			isActive: data.product.isActive
		});
		return newProduct.save().then((product, error) => {
			if(error){
				return false
			} else {
				return true
			}
		});
	// if(data.isAdmin) {
	// 	let newProduct = new Product({
	// 		name: data.product.name,
	// 		description: data.product.description,
	// 		price: data.product.price
	// 	});
	// 	return newProduct.save().then((product, error) => {
	// 		if(error){
	// 			return false
	// 		} else {
	// 			return {"message": "Product added successfully"}
	// 		}
	// 	});

	// };
		// let message = Promise.resolve('User must be ADMIN to access this!')
		// return message.then((value) => {
		// 	return {value};
		// })
};


module.exports.retrieveAllProduct = () => {
	return Product.find({}).then(result => {
		return result;
	});
};



module.exports.retrieveAllActiveProduct = () => {
	return Product.find({isActive: true}).then(result => {
		return result;
	});
};


module.exports.retrieveOneProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	});
};

module.exports.updateProduct = (data, reqParams) => {
	
			let updateProduct = {
			name: data.name,
			description: data.description,
			price: data.price,
			isActive: data.isActive
	
		};
		console.log("controller")
		console.log(data)
		console.log(reqParams.productId)
		return Product.findByIdAndUpdate(reqParams.productId, updateProduct)
			.then((product, error) => {
				if (error){
					return false
				} else {
					return true
				};
			});

	
		
}
// module.exports.updateProduct = (data) => {
// 	if(data.isAdmin) {
// 			let updateProduct = {
// 			name: data.name,
// 			description: data.description,
// 			price: data.price
// 		};
		
// 		return Product.findByIdAndUpdate(data.productId, updateProduct)
// 			.then((product, error) => {
// 				if (error){
// 					return false
// 				} else {
// 					return true
// 				};
// 			});

// 	};
// 		let message = Promise.resolve('User must be ADMIN to access this!')
// 		return message.then((value) => {
// 			return {value};
// 		})
// }

// 	return User.findById(reqParams.userId).then(result=>{
// 	if (result.isAdmin){
// 		let updateProduct = {
// 		name: reqBody.name,
// 		description: reqBody.description,
// 		price: reqBody.price
// 	};

// 	return Product.findByIdAndUpdate(reqParams.productId, updateProduct)
// 	.then((product, error) => {
// 		if (error){
// 			return false
// 		} else {
// 			return true
// 		};
// 	});
// 	} else{
// 		return false
// 	}
// })
//};

module.exports.archiveProduct = (data) => {
		if(data.isAdmin) {
		let updateActiveField = {
		isActive : false
	};
	return Product.findByIdAndUpdate(data.productId, updateActiveField)
	.then((product, error) => {
		if (error){
			return false;
		} else {
			return {"message": "Product archive successfully"};
		};
	});
}
		let message = Promise.resolve('User must be ADMIN to access this!')
		return message.then((value) => {
			return {value};
		})

};

//check product if existing 
module.exports.checkProductExist =(reqBody)=>{
	return Product.find({name:reqBody.name}).then(result => {
		if (result.length > 0){
			return true;
		} else {
			return false }
	});
};

// module.exports.archiveProduct = (reqParams, reqBody) => {
// 	let updateActiveField = {
// 		isActive : false
// 	};
// 	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField)
// 	.then((course, error) => {
// 		if (error){
// 			return false;
// 		} else {
// 			return true;
// 		};
// 	});
// };



