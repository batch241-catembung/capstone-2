const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt =require("bcrypt");
const auth = require("../auth");

//check email if existing 
module.exports.checkEmailExist =(reqBody)=>{
	return User.find({email:reqBody.email}).then(result => {
		if (result.length > 0){
			return true;
		} else {
			return false }
	});
};
//register user (isAdmin = false)
module.exports.registerUser =(reqBody)=>{
	let newUser = new User ({
		name: reqBody.name,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),
		contactNo:reqBody.contactNo,
		address:reqBody.address });

	return newUser.save().then((user, error)=>{
		if (error){
			return false;
		} else {
			return true; 
		}
	});
};
//login user Admin/Client
module.exports.loginUser =(reqBody)=>{
	return User.findOne({email: reqBody.email}).then(result=>{
		console.log(result)
		if(result==null) {
			return false
		} else {
		const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if (isPasswordCorrect) {
				return {access: auth.createAccessToken(result)} 
			} else {
				return false }
		};
	});
};
//create order User/Client(isAdmin = false) only
module.exports.createOrder = async (data, reqParams) => {
	if(data.isAdmin==false) {
	// User Collection/Table
	let isUserUpdated = await User.findById(data.userId).then(user => {
		 // user.ordersUser.push({products:[{productId: data.productId}, {productName: data.productName},{quantity: data.quantity}], totalAmount: data.totalAmount});
		user.ordersUser.push({products: [{
										productId: reqParams.productId,
										productName: data.productName,
										quantity: data.quantity }], 
							totalAmount: data.totalAmount });
		return user.save().then((user, error) => {
			if (error) {
				return false; 
			} else {
				return true; 
			};
		});
	});
	// Product Collection/Table
	let isProductUpdated = await Product.findById(reqParams.productId).then(product => {
			product.ordersProduct.push({orderId : data.userId});
			return product.save().then((product, error) => {
				if (error) {
					return false;
				} else {
					return true;
				};
			});
		});
		
		if(isUserUpdated && isProductUpdated){
			return {"message": "Order Succeed"};
		} else {
			return {"message": "Order Failed"};
		};
};
	let message = Promise.resolve('Only client user can access this!')
	return message.then((message) => {return {message};})

};
// module.exports.createOrder = async (data) => {
// 	if(data.isAdmin==false) {
// 	// User Collection/Table
// 	let isUserUpdated = await User.findById(data.userId).then(user => {
// 		 // user.ordersUser.push({products:[{productId: data.productId}, {productName: data.productName},{quantity: data.quantity}], totalAmount: data.totalAmount});
// 		user.ordersUser.push({products: [{
// 										productId: data.productId,
// 										productName: data.productName,
// 										quantity: data.quantity }], 
// 							totalAmount: data.totalAmount });
// 		return user.save().then((user, error) => {
// 			if (error) {
// 				return false; 
// 			} else {
// 				return true; 
// 			};
// 		});
// 	});
// 	// Product Collection/Table
// 	let isProductUpdated = await Product.findById(data.productId).then(product => {
// 			product.ordersProduct.push({orderId : data.userId});
// 			return product.save().then((product, error) => {
// 				if (error) {
// 					return false;
// 				} else {
// 					return true;
// 				};
// 			});
// 		});
		
// 		if(isUserUpdated && isProductUpdated){
// 			return {"message": "Order Succeed"};
// 		} else {
// 			return {"message": "Order Failed"};
// 		};
// };
// 	let message = Promise.resolve('Only client user can access this!')
// 	return message.then((message) => {return {message};})

// };
//Retrieve All User
module.exports.retrieveAllUser = () => {
	return User.find({}).then(result => {
		return result;
	});
};
//Retrieve My User Details
module.exports.retrieveUser = (data) => {
	return User.findById(data.userId).then(result => {
		return result;
	});
};
//Retrieve All User Admin
module.exports.retrieveAllUserAdmin = (data) => {
	if(data.isAdmin){
		return User.find({}).then(result => {
		return result; });
	} else {
		let message = Promise.resolve('User must be ADMIN to access this!')
		return message.then((value) => {
			return {value};
		});
	}
};
//Retrieve authenticated user’s orders
module.exports.retrieveUserOrder =(data)=>{
		return User.findById(data.userId).then(result=>{
			return result.ordersUser
	});
};
//setAdmin
module.exports.setAdmin=(data)=>{
	if(data.isAdmin) {
	//code
	let updateActiveField = {isAdmin : true};
	return User.findByIdAndUpdate(data.newAdmin, updateActiveField)
	.then((user, error) => {
		if (error){
			return false;
		} else {
			let message = Promise.resolve('Admin updated successfully')
			return message.then((message) => {return {message};})
		};
	});
};
	let message = Promise.resolve('User must be ADMIN to access this!')
	return message.then((value) => {return {value};})		

};

//retrieveallorders //not yet done
// module.exports.retrieveallorders=(data)=>{

// 	if(data.isAdmin){
// 		return User.find({ordersUser:{}}).then(result => {
// 		return result;
// 	});

// 	}
// 		let message = Promise.resolve('User must be ADMIN to access this!')
// 		return message.then((value) => {
// 			return {value};
// 		})	
// }

//retrieve my own orders
module.exports.retrievemyorders = (data) => {
	return User.findById(data.userId).then(result => {
		return result.ordersUser;
	});
};

// Retreive user details (For Frontend)
module.exports.getProfile = (userData) => {
	return User.findById(userData.id).then(result => {
		// console.log(data.userId);
		// console.log(result);
		
		if (result == null) {
			return false
		} else {
			result.password = "*****"

			// Returns the user information with the password as an empty string or asterisk.
			return result
		}
	})
};



